import logo from './logo.svg';
import './App.css';
import ComponentName from './componentName';
function App() {
  return (
    <div className="App">
      <ComponentName name="Finland"/>
      <ComponentName name="Sweden"/>
    </div>
  );
}

export default App;
